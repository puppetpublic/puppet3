# This class makes the puppet packages from backports the default
# packages.

class puppet3 {
  # RHEL4 and older cannot use puppet3
  if (($::osfamily == 'RedHat') and ($::lsbmajdistrelease < 5)) {
    fail ('Puppet3 does not run on RHEL4 or previous')
  }

  # Set preferences for wheezy to use puppet from backports
  # note that this is our version of backports
  if (($::lsbdistcodename == 'wheezy') and ($::hostname !~ /jimhenson|frankoz/)) {
    file {'/etc/apt/preferences.d/puppet3':
      ensure => present,
      mode   => '0644',
      source => 'puppet:///modules/puppet3/etc/apt/preferences.d/puppet3',
      notify => Exec['aptitude update'],
    }
    # This package suppresses warnings when puppet is run
    package { 'libaugeas-ruby': ensure => installed }
  }

  # RHEL setup
  # we now mirror PuppetLabs repository on yum.stanford.edu, and
  # make that available to all RHELish hosts via base/manifests/rpm.pp
  # As such, this whole conditional should be removed (jlent 20150227)
  if ($::osfamily == 'RedHat') {
    file { '/etc/yum.repos.d/puppet.repo':
      ensure  => present,
      content => template('puppet3/etc/yum.repos.d/puppet.repo.erb'),
      require => Base::Rpm::Import['puppetlabs-rpmkey'],
    }
    base::rpm::import { 'puppetlabs-rpmkey':
      url       => 'https://yum.puppetlabs.com/RPM-GPG-KEY-puppetlabs',
      signature => 'gpg-pubkey-4bd6ec30-4ff1e4fa',
    }
  }

  # Template for moving to puppet3.
  if ($::hostname !~ /jimhenson|frankoz1/) {
    file {'/etc/puppet/puppet3.conf':
      ensure  => present,
      mode    => '0644',
      content => template('puppet3/etc/puppet/puppet3.conf.erb'),
    }
  }
}
